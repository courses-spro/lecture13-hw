// todo: implement service calls and write function to handle routes


import { Context } from 'koa';
import { getUsers } from './service';

export async function handleUserGet(ctx: Context) {
  const users = await getUsers();
  console.log(users)
  ctx.body = users;
  ctx.status = 200;
}
