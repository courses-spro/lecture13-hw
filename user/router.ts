// TODO: create a router and bind endpoints here
import Router from 'koa-router';
import { handleUserGet } from './controller';

const router = new Router({ prefix: '/user' });

router.get('/', handleUserGet);

export default router;
